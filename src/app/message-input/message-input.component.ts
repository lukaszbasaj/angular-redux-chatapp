import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-message-input',
  templateUrl: './message-input.component.html',
  styleUrls: ['./message-input.component.scss']
})
export class MessageInputComponent implements OnInit {

  @ViewChild('sendMessage') public sendMessage: NgForm;

  constructor() { }

  ngOnInit() {
  }
  getUserDisplayName() {

  }

  send() {

  }

  disableSendBtn() {

  }

}
