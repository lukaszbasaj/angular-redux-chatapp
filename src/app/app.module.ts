import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { MatMenuModule } from '@angular/material/menu';
import { MaterialModule } from './material.module';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CardComponent } from './card/card.component';
import { CardsComponent } from './cards/cards.component';
import { MatCardModule } from '@angular/material/card';
import { CardService } from './card/card.service';
import { MessageInputComponent } from './message-input/message-input.component';
import { MatInputModule } from '@angular/material/input';
import { LoginComponent } from './auth/login/login.component';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CardComponent,
    CardsComponent,
    MessageInputComponent,
    LoginComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatMenuModule,
    MatIconModule,
    BrowserAnimationsModule,
    MaterialModule,
    MatCardModule,
    MatInputModule
  
  ],
  providers: [CardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
