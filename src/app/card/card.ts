import { User } from '../auth/user';
import { CardColor } from './card.colors';
export class Card {
  private user: User;
  private content: string;
  private userHeading: string;
  private color: CardColor;

  constructor(user: User, content: string, userHeading: string, color: CardColor) {
    this.user = user;
    this.content = content;
    this.userHeading = userHeading;
    this.color = color;
  }
}