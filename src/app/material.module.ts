import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule,
  MatCardModule,
 
  MatInputModule,
  MatToolbarModule,
} from '@angular/material';

@NgModule({
  imports: [MatButtonModule,
  MatCardModule,

  MatInputModule,
  MatToolbarModule,],
  
  exports: [MatButtonModule,
  MatCardModule,

  MatInputModule,
  MatToolbarModule,]
})


export class MaterialModule{}