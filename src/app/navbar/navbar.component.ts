import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {


  constructor(private MatIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer) { 
    MatIconRegistry.addSvgIcon('custom-login', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/login.svg'));
    MatIconRegistry.addSvgIcon('custom-logout', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/logout.svg'));

   }

  ngOnInit() {
  }

}
